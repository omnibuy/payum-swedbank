<?php

namespace Omnibuy\Swedbank;

use Payum\Core\GatewayFactoryInterface;

class SwedbankGatewayFactoryFactory
{
    public function getCallable()
    {
        return [$this, 'getGatewayFactory'];
    }

    public function getGatewayFactory(array $config, GatewayFactoryInterface $coreGatewayFactory)
    {
        return new SwedbankGatewayFactory($config, $coreGatewayFactory);
    }
}
